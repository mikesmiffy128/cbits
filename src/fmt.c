/*
 * Copyright © 2021 Michael Smith <mikesmiffy128@gmail.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#include <limits.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "../include/intdefs.h"
#include "../include/iobuf.h"
#include "../include/str.h"
#include "iobuf-internal.h"

// Note: the int formatting code is tricky but if you stare at it for long
// enough it should start to make sense

static inline int u32digits(u32 n) {
	if (n < 10) return 1;
	if (n < 100) return 2;
	if (n < 1000) return 3;
	if (n < 10000000) {
		if (n < 1000000) {
			if (n < 10000) return 4;
			return 5 + (n >= 100000);
		}
		return 7;
	}
	if (n < 1000000000) return 8 + (n >= 100000000);
	return 10;
}

static inline int u64digits(u64 n) {
	if (n < 10) return 1;
	if (n < 100) return 2;
	if (n < 1000) return 3;
	if (n < 1000000000000) {
		if (n < 100000000) {
			if (n < 1000000) {
				if (n < 10000) return 4;
				return 5 + (n >= 100000);
			}
			return 7 + (n >= 10000000);
		}
		if (n < 10000000000) {
			return 9 + (n >= 1000000000);
		}
		return 11 + (n >= 100000000000);
	}
	return 12 + u64digits(n / 1000000000000);
}

#define TEN(t) #t"0"#t"1"#t"2"#t"3"#t"4"#t"5"#t"6"#t"7"#t"8"#t"9"
static const char digits[200] =
	TEN(0) TEN(1) TEN(2) TEN(3) TEN(4) TEN(5) TEN(6) TEN(7) TEN(8) TEN(9);
#undef TEN

#define DO_FMT_UINT(space, n, i) do { \
	for (--i; n >= 100; n /= 100) { \
		int j = (n % 100) * 2; \
		space[i--] = digits[j + 1]; space[i--] = digits[j]; \
	} \
	if (n < 10) { \
		space[i] = '0' + n; \
	} \
	else { \
		n <<= 1; \
		space[i--] = digits[n + 1]; space[i] = digits[n]; \
	} \
} while (0)

#define DO_BUF_PAD(buf, mlen, rlen, c) do { \
	u32 sz; \
	for (int j = mlen - rlen; j > 0; j -= sz) { \
		sz = obuf_avail(buf); \
		if (!sz) { \
			if (!obuf_flush(buf)) return -1; \
			sz = buf->sz; \
		} \
		if (sz > mlen - rlen) sz = mlen - rlen; \
		memset(buf->b + buf->n, c, sz); buf->n += sz; \
	} \
} while (0)

#define INT_FUNCS(BITS, X, TYPE) \
int fmt_##X##_u##BITS(struct TYPE *buf, u##BITS n) { \
	int i = u##BITS##digits(n); \
	int total = i; \
	char *space = _##TYPE##_contigspace(buf, i); \
	if (!space) return -1; \
	DO_FMT_UINT(space, n, i); \
	return total; \
} \
\
int fmt_##X##_s##BITS(struct TYPE *buf, s##BITS n) { \
	int i = u##BITS##digits(n < 0 ? -n : n); \
	int total = i + (n < 0); \
	char *space = _##TYPE##_contigspace(buf, total); \
	if (!space) return -1; \
	if (n < 0) { n = -n; *space++ = '-'; } \
	DO_FMT_UINT(space, n, i); \
	return total; \
}

INT_FUNCS(32, buf, obuf)
INT_FUNCS(32, str, str)
INT_FUNCS(64, buf, obuf)
INT_FUNCS(64, str, str)

#define INT_PAD_FUNCS(BITS) \
int fmt_buf_u##BITS##pad(struct obuf *buf, u##BITS n, char c, uint mlen) { \
	int i = u##BITS##digits(n); \
	if (mlen > i) DO_BUF_PAD(buf, mlen, i, c); else mlen = i; \
	char *space = _obuf_contigspace(buf, i); \
	if (!space) return -1; \
	DO_FMT_UINT(space, n, i); \
	return mlen; \
} \
\
int fmt_str_u##BITS##pad(struct str *s, u##BITS n, char c, uint mlen) { \
	int i = u##BITS##digits(n); \
	if (mlen < i) mlen = i; \
	char *space = _str_contigspace(s, mlen); \
	if (!space) return -1; \
	if (mlen > i) { \
		memset(space, c, mlen - i); \
		space += mlen - i; \
	} \
	DO_FMT_UINT(space, n, i); \
	return mlen; \
} \
\
int fmt_buf_s##BITS##pad(struct obuf *buf, s##BITS n, char c, uint mlen) { \
	u##BITS nabs = n < 0 ? -n : n; \
	int i = u##BITS##digits(nabs) + (n < 0); \
	if (mlen > i) DO_BUF_PAD(buf, mlen, i, c); else mlen = i; \
	char *space = _obuf_contigspace(buf, i); \
	if (!space) return -1; \
	if (n < 0) *space++ = '-'; \
	DO_FMT_UINT(space, nabs, i); \
	return mlen; \
} \
\
int fmt_str_s##BITS##pad(struct str *s, s##BITS n, char c, uint mlen) { \
	u##BITS nabs = n < 0 ? -n : n; \
	int i = u##BITS##digits(nabs) + (n < 0); \
	if (mlen < i) mlen = i; \
	char *space = _str_contigspace(s, mlen); \
	if (!space) return -1; \
	if (mlen > i) { \
		memset(space, c, mlen - i); \
		space += mlen - i; \
	} \
	if (n < 0) *space = '-'; \
	DO_FMT_UINT(space, nabs, i); \
	return mlen; \
}

INT_PAD_FUNCS(32)
INT_PAD_FUNCS(64)

#define INT_FIXED_FUNCS(BITS) \
int fmt_fixed_u##BITS(char *buf, u##BITS n) { \
	int i = u##BITS##digits(n); \
	int total = i; \
	DO_FMT_UINT(buf, n, i); \
	return total; \
} \
\
int fmt_fixed_s##BITS(char *buf, s##BITS n) { \
	int i = u##BITS##digits(n < 0 ? -n : n); \
	int total = i + (n < 0); \
	if (n < 0) { n = -n; *buf++ = '-'; } \
	DO_FMT_UINT(buf, n, i); \
	return total; \
}

INT_FIXED_FUNCS(64)
INT_FIXED_FUNCS(32)

int fmt_buf_str(struct obuf *buf, const struct str *s) {
	return obuf_putstr(buf, s) ? str_len(s) : -1;
}

int fmt_str_str(struct str *s, const struct str *s2) {
	return str_appendstr(s, s2) ? str_len(s2) : -1;
}

// XXX strpad functions don't do unicode / runewidth

int fmt_buf_strpad(struct obuf *buf, const struct str *s, char c, uint mlen) {
	uint slen = str_len(s);
	if (mlen > slen) DO_BUF_PAD(buf, mlen, slen, c); else mlen = slen;
	return fmt_buf_str(buf, s) == -1 ? -1 : mlen;
}

int fmt_str_strpad(struct str *s, const struct str *s2, char c, uint mlen) {
	uint slen = str_len(s2);
	char *space;
	if (mlen > slen) {
		space = _str_contigspace(s, mlen);
		if (!space) return -1;
		memset(space, c, mlen - slen);
	}
	else {
		mlen = slen;
		space = _str_contigspace(s, mlen);
		if (!space) return -1;
	}
	memcpy(space + mlen - slen, s2->data, str_len(s2));
	return mlen;
}

// vi: sw=4 ts=4 noet tw=80 cc=80
