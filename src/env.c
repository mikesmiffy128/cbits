/* This file is dedicated to the public domain. */

#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "../include/intdefs.h"
#include "../include/vec.h"
#include "../include/str.h"
#include "../include/env.h"

// TODO think of some ways to clean this up later

static char **find_env(char **env, const char *var, u64 len) {
	for (; *env; ++env) {
		if (!strncmp(*env, var, len) && (*env)[len] == '=') break;
	}
	return env;
}

static void clearall(struct env *v) {
	if (v->sz) {
		for (char **p = v->data; p - v->data < v->sz - 1; ++p) free(*p);
		v->sz = 0;
	}
}

bool env_init(struct env *v) {
	clearall(v);
	for (char **p = environ; *p; ++p) {
		struct str s = {0};
		if (!str_clear(&s)) goto e;
		if (!str_append0t(&s, *p)) { free(s.data); goto e; }
		if (!vec_push(v, s.data)) { free(s.data); goto e; }
	}
	if (!vec_push(v, 0)) goto e;
	return true;

e:	clearall(v);
	return false;
}

bool env_initempty(struct env *v) {
	clearall(v);
	return vec_push(v, 0);
}

const char *env_get(struct env *v, const char *var) {
	u64 len = strlen(var);
	char **ret = find_env(v->data, var, len);
	if (*ret) return *ret + len + 1;
	return 0;
}

bool env_put(struct env *v, const char *var, const char *val) {
	u64 len = strlen(var);
	if (val) {
		u64 vallen = strlen(var);
		char *new = malloc(len + sizeof('=') + vallen + sizeof('\0'));
		if (!new) return false;
		char **e = find_env(v->data, var, len);
		if (e) {
			free(*e);
			*e = new;
		}
		else if (vec_push(v, 0)) {
			v->data[v->sz - 2] = new;
		}
		else {
			free(new);
			return false;
		}
		memcpy(new, var, len);
		new[len] = '=';
		memcpy(new + len + 1, val, vallen);
		new[len + 1 + vallen] = '\0';
		return true;
	}
	// else unset the variable
	char **e = find_env(v->data, var, len);
	if (*e) {
		free(*e);
		memmove(e, e + 1, v->sz-- - (e - v->data));
	}
	return true;
}

// vi: sw=4 ts=4 noet tw=80 cc=80
