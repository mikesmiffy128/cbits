/* This file is dedicated to the public domain. */

#include <ctype.h>

#include "../include/intdefs.h"
#include "../include/rune.h"
#include "../include/utf.h"

uint strwidth(const char *s) {
	uint total = 0;
	// XXX not parsing grapheme clusters/zwj (terminal support is assumed to
	// suck anyway, but this can be fixed if ever needed)
	for (rune r; *s; s = utf_get(s, &r), total += runewidth(r));
	return total;
}

uint strwidth_colour(const char *s) {
	uint total = 0;
	rune r;
	for (const char *next = utf_get(s, &r); *s; next = utf_get(s, &r)) {
		if (r == '\x1B') {
			switch (*++s) {
				case '\0': return total; // broken sequence, no point worrying
				case '[': while (*++s && !isalpha(*s)); break; // colours, etc.
				case ']': while (*++s && *s != '\x07'); break; // title
				case '(': case ')': ++s; // other stuff
				// XXX anything important missing?
			}
			++s;
		}
		else {
			s = next;
			total += runewidth(r);
		}
	}
	return total;
}

// vi: sw=4 ts=4 noet tw=80 cc=80
