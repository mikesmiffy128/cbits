/* This file is dedicated to the public domain. */
/* Some messages were derived from qmail, which is also in the public domain. */

#include <errno.h>

const char *errorstring(int e) {
	switch (e) {
		case 0: return "no error code set";
		case E2BIG: return "argument list too long";
		case EACCES: return "access denied";
		case EADDRINUSE: return "address already used";
		case EADDRNOTAVAIL: return "address not available";
#ifdef EADV
		case EADV: return "advertise error";
#endif
		case EAFNOSUPPORT: return "address family not supported";
#if EAGAIN != EWOULDBLOCK
		case EAGAIN: return "temporary error";
#endif
		case EALREADY: return "connection already in progress";
#ifdef EAUTH
		case EAUTH: return "authentication error";
#endif
		case EBADF: return "invalid file descriptor";
		case EBADMSG: return "bad message type";
#ifdef EBADRPC
		case EBADRPC: return "invalid RPC structure";
#endif
		case EBUSY: return "device busy";
		case ECANCELED: return "cancelled";
		case ECHILD: return "no child processes";
#ifdef ECOMM
		case ECOMM: return "communication error";
#endif
		case ECONNABORTED: return "connection aborted";
		case ECONNREFUSED: return "connection refused";
		case ECONNRESET: return "connection reset";
		case EDEADLK: return "operation would cause deadlock";
		case EDESTADDRREQ: return "destination address required";
#ifdef ESTRPIPE
		case ESTRPIPE: return "pipe error"; // ???
#endif
		case EDOM: return "parameter out of range";
		case EDQUOT: return "disk quota exceeded";
		case EEXIST: return "file already exists";
		case EFAULT: return "bad address";
		case EFBIG: return "file is too big";
#ifdef EFTYPE
		case EFTYPE: return "bad file type";
#endif
#ifdef EHOSTDOWN
		case EHOSTDOWN: return "host down";
#endif
		case EHOSTUNREACH: return "host unreachable";
		case EIDRM: return "identifier removed";
		case EINPROGRESS: return "operation in progress";
		case EINTR: return "interrupted system call";
		case EINVAL: return "invalid argument";
		case EIO: return "IO error";
		case EISCONN: return "already connected";
		case EISDIR: return "is a directory";
		case ELOOP: return "too many symbolic link levels";
		case EMFILE: return "process cannot open more files";
		case EMLINK: return "too many links";
		case EMSGSIZE: return "message too long";
#ifdef EMULTIHOP
		case EMULTIHOP: return "multihop attempted";
#endif
		case ENAMETOOLONG: return "file name too long";
#ifdef ENEEDAUTH
		case ENEEDAUTH: return "not authenticated";
#endif
		case ENETDOWN: return "network down";
		case ENETRESET: return "network reset";
		case ENETUNREACH: return "network unreachable";
		case ENFILE: return "system cannot open more files";
		case ENOBUFS: return "out of buffer space";
		case ENODEV: return "no such device";
		case ENOENT: return "file or directory does not exist";
		case ENOEXEC: return "invalid executable format";
		case ENOLCK: return "no locks available";
#ifdef ENOLINK
		case ENOLINK: return "link severed";
#endif
		case ENOMEM: return "out of memory";
		case ENOMSG: return "no message of desired type";
#ifdef ENONET
		case ENONET: return "not on the network";
#endif
		case ENOPROTOOPT: return "protocol not available";
		case ENOSPC: return "out of disk space";
#ifdef ENOSR
		case ENOSR: return "out of stream resources";
#endif
#ifdef ENOSTR
		case ENOSTR: return "not a stream device";
#endif
		case ENOSYS: return "unimplemented system call";
#ifdef ENOTBLK
		case ENOTBLK: return "not a block device";
#endif
		case ENOTCONN: return "not connected";
		case ENOTDIR: return "not a directory";
		case ENOTEMPTY: return "directory not empty";
		case ENOTSOCK: return "not a socket";
#ifdef ENOTRECOVERABLE
		case ENOTRECOVERABLE: return "irrecoverable mutex state";
#endif
		case ENOTTY: return "not a tty";
		case ENXIO: return "device not configured";
		case EOPNOTSUPP: return "operation not supported";
		case EOVERFLOW: return "value too large for structure";
#ifdef EOWNERDEAD
		case EOWNERDEAD: return "mutex owner died";
#endif
		case EPERM: return "permission denied";
#ifdef EPFNOSUPPORT
		case EPFNOSUPPORT: return "protocol family not supported";
#endif
		case EPIPE: return "pipe has no readers";
#ifdef EPROCLIM
		case EPROCLIM: return "too many processes";
#endif
#ifdef EPROCUNAVAIL
		case EPROCUNAVAIL: return "bad procedure for program";
#endif
#ifdef EPROGMISMATCH
		case EPROGMISMATCH: return "program version mismatch";
#endif
#ifdef EPROGUNAVAIL
		case EPROGUNAVAIL: return "RPC program unavailable";
#endif
		case EPROTONOSUPPORT: return "protocol not supported";
		case EPROTO: return "protocol error";
		case EPROTOTYPE: return "incorrect protocol type";
		case ERANGE: return "result out of range";
#ifdef EREMCHG
		case EREMCHG: return "remote address changed";
#endif
#ifdef EREMOTE
		case EREMOTE:
#ifdef ERREMOTE // this is a little tricky :(
			return "too many levels of remote in path";
		case ERREMOTE:
#endif
			return "object not local";
#endif
#ifdef EREMOTEIO
		case EREMOTEIO: return "remote IO error";
#endif
		case EROFS: return "filesystem is read-only";
#ifdef ERPCMISMATCH
		case ERPCMISMATCH: return "RPC version mismatch";
#endif
#ifdef ESHUTDOWN
		case ESHUTDOWN: return "socket was shut down";
#endif
#ifdef ESOCKTNOSUPPORT
		case ESOCKTNOSUPPORT: return "socket type not supported";
#endif
		case ESPIPE: return "cannot seek in a stream";
		case ESRCH: return "no such process";
#ifdef ESRMNT
		case ESRMNT: return "srmount error";
#endif
		case ESTALE: return "stale NFS file handle";
		case ETIMEDOUT: return "connection timed out";
#ifdef ETIME
		case ETIME: return "stream ioctl timed out";
#endif
#ifdef ETOOMANYREFS
		case ETOOMANYREFS: return "too many references";
#endif
		case ETXTBSY: return "text segment in use";
#ifdef EUSERS
		case EUSERS: return "too many users";
#endif
		case EWOULDBLOCK: return "operation would block";
		case EXDEV: return "cross-device link";
		default: return "unknown error";
	}
}

// vi: sw=4 ts=4 noet tw=80 cc=80
