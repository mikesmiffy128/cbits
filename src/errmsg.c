/* This file is dedicated to the public domain. */

#include <stdlib.h>

#include "../include/iobuf.h"
#include "../include/errorstring.h"
#include "../include/noreturn.h"

// pretty much a ripoff of strerr from djb... although it turns out there isn't
// really a nicer way of doing this
void _errmsg_warn(const char *s1, const char *s2, const char *s3,
		const char *s4, const char *s5, const char *s6, const char *s7,
		const char *s8, int err) {
	obuf_put0t(buf_err, getprogname());
	obuf_putbytes(buf_err, (char []){':', ' '}, 2);
	if (s1) obuf_put0t(buf_err, s1); if (s2) obuf_put0t(buf_err, s2);
	if (s3) obuf_put0t(buf_err, s3); if (s4) obuf_put0t(buf_err, s4);
	if (s5) obuf_put0t(buf_err, s5); if (s6) obuf_put0t(buf_err, s6);
	if (s7) obuf_put0t(buf_err, s7); if (s8) obuf_put0t(buf_err, s8);
	if (err > -1) {
		if (s1) obuf_putbytes(buf_err, (char []){':', ' '}, 2);
		obuf_put0t(buf_err, errorstring(err));
	}
	obuf_putc(buf_err, '\n');
	obuf_flush(buf_err);
	// if we can't put out an error message, tough luck :(
	obuf_reset(buf_err);
}

noreturn _errmsg_die(const char *s1, const char *s2, const char *s3,
		const char *s4, const char *s5, const char *s6, const char *s7,
		const char *s8, int err, int status) {
	_errmsg_warn(s1, s2, s3, s4, s5, s6, s7, s8, err);
	exit(status);
}

const char msg_note[]	= "note: ";
const char msg_warn[]	= "warning: ";
const char msg_temp[]	= "temporary error: ";
const char msg_error[]	= "error: ";
const char msg_crit[]	= "critical: ";
const char msg_fatal[]	= "fatal: ";

// vi: sw=4 ts=4 noet tw=80 cc=80
