/* This file is dedicated to the public domain. */

#include <stdbool.h>

#include "../../include/errmsg.h"
#include "../../include/fmt.h"
#include "../../include/iobuf.h"
#include "../../include/opt.h"

USAGE("[-s] name");

static void w(const char *s) {
	if (!obuf_put0t(buf_out, s)) {
		errmsg_die(200, msg_fatal, "couldn't write output");
	}
}

static short r(void) {
	short c = ibuf_getc(buf_in);
	if (c == -1) errmsg_die(200, msg_fatal, "couldn't read input");
	return c;
}

int main(int argc, char *argv[]) {
	bool sflag = false;
	FOR_OPTS(argc, argv, { case 's': sflag = true; });
	if (argc != 1) { errmsg_warnx("expected 1 argument"); usage(); }

	if (sflag) w("static ");
	w("const char "); w(argv[0]); w("[] = {");
	short c;
	while ((c = r()) != IOBUF_EOF) {
		if (fmt_buf_u32(buf_out, c) == -1) {
			errmsg_die(200, "couldn't format output");
		}
		w(",");
	}
	w("};\n");
	if (!obuf_flush(buf_out)) {
		errmsg_die(200, msg_fatal, "couldn't perform final write");
	}

	return 0;
}

// vi: sw=4 ts=4 noet tw=80 cc=80
