/* This file is dedicated to the public domain. */

#include <stdbool.h>

#include "../../include/errmsg.h"
#include "../../include/iobuf.h"
#include "../../include/opt.h"

USAGE("[-s] name value");

static void w(const char *s) {
	if (!obuf_put0t(buf_out, s)) {
		errmsg_die(200, msg_fatal, "couldn't write output");
	}
}

int main(int argc, char *argv[]) {
	bool sflag = false;
	FOR_OPTS(argc, argv, { case 's': sflag = true; });
	if (argc != 2) { errmsg_warnx("expected 2 arguments"); usage(); }

	if (sflag) w("static ");
	w("const char *const "); w(argv[0]); w(" = \"");
	char c;
	while (c = *((argv[1])++)) {
		w("\\");
		char octal[4];
		octal[3] = 0;
		octal[2] = '0' + (c & 7); c >>= 3;
		octal[1] = '0' + (c & 7); c >>= 3;
		octal[0] = '0' + (c & 7);
		w(octal);
	}
	w("\";\n");
	if (!obuf_flush(buf_out)) {
		errmsg_die(200, msg_fatal, "couldn't perform final write");
	}

	return 0;
}

// vi: sw=4 ts=4 noet tw=80 cc=80
