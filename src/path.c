/* This file is dedicated to the public domain. */

#include <stdbool.h>
#include <string.h>
#include <unistd.h>

#include "../include/env.h"
#include "../include/str.h"

bool path_isfull(const char *prog) { return !!strchr(prog, '/'); }

const char *path_search(const char *path, const char *prog) {
	int savederrno = 0;
	struct str s = {0};
	do {
		char *next = strchrnul(path, ':');
		if (path != next) { // be lenient on extra separators and stuff
			if (!str_clear(&s)) break;
			if (!str_appendbytes(&s, path, next - path)) break;
			if (!str_appendc(&s, '/')) break;
			if (!str_append0t(&s, prog)) break;

			// Before you ask: isn't access() before exec() a TOCTOU issue?
			//                 shouldn't you use open/fexecv?
			// Sadly, that doesn't work for many reasons:
			// - you can't open/fexecve because that would leak the fd
			// - you can't set O_CLOEXEC because scripts with shebangs would
			//   fail (file has to be accessible via /dev/fd)
			// - running scripts via /dev/fd could cause issues with chroots
			// - also, passing something other than the file path means scripts
			//   can't get something like $0
			// - even if you parse the shebang manually in userspace, stuff like
			//   binfmt_misc would break
			// - TOCTOU is normally unavoidable for shebanged files anyway
			//   because the kernel just passes the file path to the interpreter
			// So we have path_search and, well, it's basically fine anyway.
			if (access(s.data, R_OK | X_OK) == 0) return s.data;
			if (errno != ENOENT) {
				if (errno != EACCES && errno != EPERM && errno != EISDIR &&
						errno != ENOTDIR) {
					break;
				}
				savederrno = errno;
			}
		}
		if (!*next) {
			if (savederrno) errno = savederrno;
			break;
		}
		path = next + 1;
	} while (*path);

	free(s.data);
	return 0;
}

// vi: sw=4 ts=4 noet tw=80 cc=80
