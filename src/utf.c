/*
 * Copyright © 2019 Michael Smith <mikesmiffy128@gmail.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#include "../include/intdefs.h"
#include "../include/rune.h"

const char *utf_get(const char *s, rune *r) {
	switch ((unsigned char)*s >> 3) {
		case 0x00:                                  // already at end of string
			*r = 0;
			return s;
		case 0x01: case 0x02: case 0x03: case 0x04: // 0xxxxyyy (ASCII)
		case 0x05: case 0x06: case 0x07: case 0x08:
		case 0x09: case 0x0A: case 0x0B: case 0x0C:
		case 0x0D: case 0x0E: case 0x0F: case 0x1F: // 11111yyy (invalid utf8)
			*r = *s;
			return s + 1;
		case 0x10: case 0x11: case 0x12: case 0x13: // 10xxxyyy (continuation)
		case 0x14: case 0x15: case 0x16: case 0x17:
			goto ec;
		case 0x18: case 0x19: case 0x1A: case 0x1B: // 110xxyyy
			*r = (*s & 0x1F) <<  6;
			goto n1;
		case 0x1C: case 0x1D:                       // 1110xyyy
			*r = (*s & 0x0F) << 12;
			goto n2;
		case 0x1E:                                  // 11110yyy
			*r = (*s & 0x07) << 18;
			goto n3;
	}
n3: if ((*++s & 0xC0) != 0x80) goto e0; *r |= (*s & 0x3F) << 12;
n2: if ((*++s & 0xC0) != 0x80) goto e0; *r |= (*s & 0x3F) <<  6;
n1: if ((*++s & 0xC0) != 0x80) goto e0; *r |=  *s & 0x3F;
	return s + 1;
ec: while ((*++s & 0xC0) == 0x80);
e0: *r = 0xFFFD;
	return s;
}

uint utf_len(const char *s) {
	uint total = 0;
	for (rune r; *s; s = utf_get(s, &r), ++total);
	return total;
}

// vi: sw=4 ts=4 noet tw=80 cc=80
