/* This file is dedicated to the public domain. */

#include <stdbool.h>

#include "../include/intdefs.h"
#include "../include/rune.h"
#include "rune-tables.h"

static bool rw_intable(rune r, const struct rw_range tab[], uint sz) {
	if (r < tab[0].start) return false;
	int b = 0, t = sz - 1;
	while (t >= b) {
		int m = (b + t) / 2;
		if (tab[m].end < r) b = m + 1;
		else if (tab[m].start > r) t = m - 1;
		else return true;
	}
	return false;
}

int runewidth(uint r) {
#define INTABLE(name) \
	rw_intable(r, rw_##name, sizeof(rw_##name) / sizeof(rw_##name[0]))

	if (r > 0x10FFF || INTABLE(nonprint) || INTABLE(combining) ||
			INTABLE(notassigned)) return 0;
	if (INTABLE(doublewidth) || INTABLE(emoji)) return 2;
	return 1;

#undef INTABLE
}

// vi: sw=4 ts=4 noet tw=80 cc=80
