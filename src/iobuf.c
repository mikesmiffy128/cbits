/*
 * Copyright © 2021 Michael Smith <mikesmiffy128@gmail.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>
#include <sys/uio.h>
#include <unistd.h>

#include "../include/iobuf.h"
#include "../include/intdefs.h"
#include "../include/str.h"

// TODO: can further optimise when space is already given by readv/writev-ing
// directly and using the buffer space only for extra data... maybe??????

struct ibuf *const buf_in  = IBUF(0, 8192);
struct obuf *const buf_out = OBUF(1, 8192);
struct obuf *const buf_err = OBUF(2, 4096);

void obuf_reset(struct obuf *buf) { buf->n = 0; }

bool obuf_flush(struct obuf *buf) {
	uint off = 0;
	while (buf->n) {
		ssize_t nwritten = write(buf->fd, buf->b + off, buf->n);
		if (nwritten == -1) {
			if (errno == EINTR) continue;
			// XXX: errors may discard; unclear whether keeping the buffer
			// content would be better
			obuf_reset(buf);
			return false;
		}
		off += nwritten; buf->n -= nwritten;
	}
	return true;
}

uint obuf_avail(const struct obuf *buf) { return buf->sz - buf->n; }

uint obuf_fill(struct obuf *buf, const char *x, uint n) {
	uint avail = obuf_avail(buf);
	if (avail < n) n = avail;
	if (!n) return 0;
	memcpy(buf->b + buf->n, x, n);
	buf->n += n;
	return n;
}

bool obuf_putc(struct obuf *buf, char c) {
	if (!obuf_avail(buf) && !obuf_flush(buf)) return false;
	buf->b[buf->n++] = c;
	return true;
}

bool obuf_putbytes(struct obuf *buf, const char *bytes, uint n) {
	while (n) {
		if (!obuf_avail(buf) && !obuf_flush(buf)) return false;
		uint filled = obuf_fill(buf, bytes, n);
		bytes += filled; n -= filled;
	}
	return true;
}

bool obuf_putstr(struct obuf *buf, const struct str *s) {
	return obuf_putbytes(buf, s->data, str_len(s));
}

// TODO: do this properly, without strlen, once I'm not lazy
bool obuf_put0t(struct obuf *buf, const char *s) {
	return obuf_putbytes(buf, s, strlen(s));
}

static int fill(struct ibuf *buf) {
	buf->r = 0;
	int ret;
	while ((ret = read(buf->fd, buf->b, buf->sz)) == -1) {
		if (errno != EINTR) return -1;
	}
	buf->w = ret;
	return ret;
}

short ibuf_getc(struct ibuf *buf) {
	if (buf->r == buf->w) {
		int nread = fill(buf);
		if (nread == -1) return -1;
		if (nread == 0) return IOBUF_EOF;
	}
	return (uchar)buf->b[buf->r++]; // cast to make sure not to sign-extend
}

int ibuf_getbytes(struct ibuf *buf, void *_bytes, int n) {
	char *bytes = _bytes;
	for (int m = 0; m < n;) {
		if (buf->r == buf->w) {
			int nread = fill(buf);
			if (nread == -1) return -1;
			if (nread == 0) return m;
		}
		uint len = buf->w - buf->r;
		if (len > n - m) len = n - m;
		memcpy(bytes + m, buf->b + buf->r, len);
		buf->r += len;
		m += len;
	}
	return n;
}

int ibuf_getstr(struct ibuf *buf, struct str *s, char term) {
	// not sure if this is fast, but good enough for now...
	for (int total = 0;; ++total) {
		short c = ibuf_getc(buf);
		if (c == -1) return -1;
		// note: this is an unexpected EOF; it's up to the caller to distinguish
		// this case if they want to
		if (c == IOBUF_EOF) return total;
		if (!str_appendc(s, c)) return -1;
		if (c == term) return total + 1; // include term!
	}
}

// this is kinda a janky interface, it's just used for fmt and not public api
char *_obuf_contigspace(struct obuf *buf, uint n) {
	if (buf->sz < n) { errno = ENOBUFS; return 0; }
	if (buf->sz - buf->n < n && !obuf_flush(buf)) return 0;

	char *ret = buf->b + buf->n;
	buf->n += n;
	return ret;
}

// vi: sw=4 ts=4 noet tw=80 cc=80
