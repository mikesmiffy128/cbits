/* This file is dedicated to the public domain. */

#include "test.h"
{.desc = "dynamic string buffers"};

#include <string.h>
#include "../include/str.h"

static inline void initstr(struct str *s) {
	if (!str_clear(s)) { perror(0); exit(1); }
}

TEST("a string buffer should be initialised with a single null byte") {
	struct str s = {0};
	initstr(&s);
	return s.sz == 1 && s.data[0] == '\0';
}

TEST("appending a string buffer to a string buffer should work as expected") {
	struct str s = {0};
	initstr(&s);
	// XXX internal structure hackery for brevity
	struct str s2 = {3, 3, "ab"};
	struct str s3 = {3, 3, "cd"};
	if (!str_appendstr(&s, &s2) || !str_appendstr(&s, &s3)) {
		perror(0);
		return false;
	}
	return !strcmp(s.data, "abcd");
}

TEST("appending a null-terminated string to a string buffer should work as expected") {
	struct str s = {0};
	initstr(&s);
	struct str s2 = {3, 3, "ab"};
	if (!str_appendstr(&s, &s2) || !str_append0t(&s, "cdefgh") ||
			!str_append0t(&s, "ijkl")) {
		perror(0);
		return false;
	}
	return !strcmp(s.data, "abcdefghijkl");
}

TEST("appending a character to a string buffer should work as expected") {
	struct str s = {0};
	initstr(&s);
	if (!str_append0t(&s, "abcd") || !str_appendc(&s, 'e')) {
		perror(0);
		return false;
	}
	return !strcmp(s.data, "abcde");
}

TEST("appending a series of bytes to a string buffer should work as expected") {
	struct str s = {0};
	initstr(&s);
	if (!str_appendbytes(&s, "abcd", 4) || !str_appendbytes(&s, "efgh", 4)) {
		perror(0);
		return false;
	}
	return !strcmp(s.data, "abcdefgh");
}

// somewhat trivial, but meh
TEST("a string's length should be 1 less than the underlying vector size") {
	struct str s = {6, 6, "hello"};
	return str_len(&s) == 5;
}

// vi: sw=4 ts=4 noet tw=80 cc=80
