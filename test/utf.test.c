/* This file is dedicated to the public domain. */

#include "test.h"
{.desc = "utf-8 <-> rune conversion"};

#include <stdbool.h>

#include "../src/rune.c"
#include "../src/utf.c"

TEST("iterating through an ASCII string should work") {
	const char *s = "Hello!";
	const char *p1 = s, *p2 = p1;
	for (rune r; *s;) {
		s = utf_get(s, &r);
		if (r != *p2++) return false;
	}
	return s == p2 && !*p2;
}

TEST("iterating though a UTF-8 string should work") {
	const char *s = "κόσμε", *sp = s;
	const rune runes[] = {0x3BA, 0x1F79, 0x3C3, 0x3BC, 0x3B5, 0}, *rp = runes;
	for (rune r; *sp;) {
		sp = utf_get(sp, &r);
		if (!*rp || r != *rp++) return false;
	}
	return !*rp && !*sp;
}

TEST("unexpected bytes should produce a replacement character") {
	const char *s = "κ\x80όσμε", *sp = s;
	const rune runes[] = {0x3BA, 0xFFFD, 0x1F79, 0x3C3, 0x3BC, 0x3B5, 0};
	const rune *rp = runes;
	for (rune r; *sp;) {
		sp = utf_get(sp, &r);
		if (!*rp || r != *rp++) return false;
	}
	return !*rp && !*sp;
}

TEST("truncated utf8 sequences should produce a replacement character") {
	const char *s = "\xE0\x82""abc", *sp = s;
	const rune runes[] = {0xFFFD, 'a', 'b', 'c', 0};
	const rune *rp = runes;
	for (rune r; *sp;) {
		sp = utf_get(sp, &r);
		if (!*rp || r != *rp++) return false;
	}
	return !*rp && !*sp;
}

// vi: sw=4 ts=4 noet tw=80 cc=80
