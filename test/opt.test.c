/* This file is dedicated to the public domain. */

#include "test.h"
{.desc = "command-line option parsing"};

#include <string.h>
#include "../include/opt.h"

USAGE("");

// just eat all error output from opt.h
bool obuf_put0t(struct obuf *b, const char *c) { return true; }
bool obuf_putc(struct obuf *b, char c) { return true; }
bool obuf_flush(struct obuf *b) { return true; }
struct obuf *obuf_err = 0;

TEST("an invalid option should cause an error", .expected_exit = 1) {
	int argc = 2;
	char *_argv[] = {"myprogram", "-af", 0};
	char **argv = _argv;
	FOR_OPTS(argc, argv, {
		case 'f': break;
		case 'd': break;
	});
	return false;
}

TEST("a missing option argument should cause an error", .expected_exit = 1) {
	int argc = 2;
	char *_argv[] = {"myprogram", "-a", 0};
	char **argv = _argv;
	FOR_OPTS(argc, argv, {
		case 'a': (void)OPTARG(argc, argv); break;
	});
	return false;
}

TEST("joined option arguments should be correctly parsed") {
	int argc = 2;
	char *_argv[] = {"myprogram", "-aargument", 0};
	char **argv = _argv;
	FOR_OPTS(argc, argv, {
		case 'a': return !strcmp(OPTARG(argc, argv), "argument");
	});
	return false;
}

TEST("separate option arguments should be correctly parsed") {
	int argc = 3;
	char *_argv[] = {"myprogram", "-a", "argument", 0};
	char **argv = _argv;
	FOR_OPTS(argc, argv, {
		case 'a': return !strcmp(OPTARG(argc, argv), "argument");
	});
	return false;
}

TEST("clustered options should be handled correctly") {
	int argc = 2;
	char *_argv[] = {"myprogram", "-abc", 0};
	char **argv = _argv;
	int good = 0;
	FOR_OPTS(argc, argv, {
		case 'a': ++good; break;
		case 'b': ++good; break;
		case 'c': ++good; break;
	});
	return good == 3;
}

TEST("separate options should be handled correctly") {
	int argc = 4;
	char *_argv[] = {"myprogram", "-a", "-b", "-c", 0};
	char **argv = _argv;
	int good = 0;
	FOR_OPTS(argc, argv, {
		case 'a': ++good; break;
		case 'b': ++good; break;
		case 'c': ++good; break;
	});
	return good == 3;
}

TEST("options should end after `--`") {
	int argc = 4;
	char *_argv[] = {"myprogram", "-a", "--", "-b", 0};
	char **argv = _argv;
	int good = 0;
	FOR_OPTS(argc, argv, {
		case 'a': ++good; break;
		case 'b': return false;
	});
	return good == 1 && !strcmp(*argv, "-b");
}

TEST("options should end upon an obvious non-option") {
	int argc = 4;
	char *_argv[] = {"myprogram", "-a", "somethingelse", "-b", 0};
	char **argv = _argv;
	int good = 0;
	FOR_OPTS(argc, argv, {
		case 'a': ++good; break;
		case 'b': return false;
	});
	return good == 1 && !strcmp(*argv, "somethingelse");
}

TEST("a single hyphen should be considered a non-option") {
	int argc = 4;
	char *_argv[] = {"myprogram", "-a", "-", "-b", 0};
	char **argv = _argv;
	int good = 0;
	FOR_OPTS(argc, argv, {
		case 'a': ++good; break;
		case 'b': return false;
	});
	return good == 1 && !strcmp(*argv, "-");
}

// vi: sw=4 ts=4 noet tw=80 cc=80
