/* This file is dedicated to the public domain. */

#include "test.h"
{.desc = "the hashtable implementation"};

#include <stdio.h>
#include <string.h>

#include "../include/intdefs.h"
#include "../include/basichashes.h"
#include "../include/table.h"

uint hash_collide(uint i) { return 1; }

DECL_TABLE(static, testtab, int, int)
DEF_TABLE(static, testtab, hash_int, table_ideq, table_scalarmemb)
struct kv { int k, v; };
#define kv_k(p) ((p)->k)
DECL_TABLE(static, collidetab, int, struct kv)
DEF_TABLE(static, collidetab, hash_collide, table_ideq, kv_k)

static void die(void) { perror("table.test"); exit(1); }

#define setup_table(t, name) do { \
	if (!table_init_##name(&t)) die(); \
	/* make initial memory consistent (so magic number tests don't fluke) */ \
	memset(t.data, 'X', t.sz * sizeof(*t.data)); \
} while (0)

TEST("inserting into a hashtable should allow retrieving the value") {
	struct table_testtab t = {0};
	setup_table(t, testtab);
	int *p = table_put_testtab(&t, 97);
	if (!p) die();
	*p = 97;
	p = table_get_testtab(&t, 97);
	return p && *p == 97;
}

TEST("colliding hashes should cause probing") {
	struct table_collidetab t = {0};
	setup_table(t, collidetab);
	struct kv *p1 = table_put_collidetab(&t, 97);
	if (!p1) die();
	p1->k = 97; p1->v = 15;
	struct kv *p2 = table_put_collidetab(&t, 98);
	if (!p2) die();
	p2->k = 98; p2->v = 16;
	p1 = table_get_collidetab(&t, 97);
	p2 = table_get_collidetab(&t, 98);
	return p1 && p2 && p1->v == 15 && p2->v == 16;
}

TEST("rehashing should bucket everything correctly") {
	struct table_testtab t = {0};
	setup_table(t, testtab);
	int *p1 = table_put_testtab(&t, 97);
	if (!p1) die();
	int *p2 = table_put_testtab(&t, 98);
	if (!p2) die();
	*p1 = 97; *p2 = 98;
	if (!_table_grow_testtab(&t)) die();
	p1 = table_get_testtab(&t, 97);
	p2 = table_get_testtab(&t, 98);
	return p1 && p2 && *p1 == 97 && *p2 == 98;
}

TEST("displaced entries should be overridden when appropriate") {
	struct table_collidetab t = {0};
	setup_table(t, collidetab);
	struct kv *p1 = table_put_collidetab(&t, 97);
	if (!p1) die();
	p1->k = 97; p1->v = 15;
	struct kv *p2 = table_put_collidetab(&t, 98);
	if (!p2) die();
	p2->k = 98; p2->v = 16;
	p1 = table_del_collidetab(&t, 97);
	p2 = table_get_collidetab(&t, 98);
	struct kv *p3 = table_put_collidetab(&t, 98);
	return p2 && p3 == p2;
}

TEST("putget should correctly indicate whether an entry already existed") {
	struct table_testtab t = {0};
	setup_table(t, testtab);
	int *p1 = table_put_testtab(&t, 97);
	if (!p1) die();
	*p1 = 97;
	bool isnew;
	int *p2 = table_putget_testtab(&t, 97, &isnew);
	if (!p2) die();
	if (isnew) return false;
	p2 = table_putget_testtab(&t, 96, &isnew);
	if (!p2) die();
	return isnew;
}

TEST("the table should perform well at reasonably large sizes",
		.timeout = 1200) { // works on my machine, smile
	struct table_testtab t;
	setup_table(t, testtab);
	int max = 2 * 1024 * 1024;
	for (int i = 0; i < max; ++i) {
		int *ent = table_put_testtab(&t, i);
		if (!ent) die();
		*ent = i;
	}
	return true;
}

TEST("a large number of entries should be retreivable without issues",
		.timeout = 5000) {
	struct table_testtab t;
	setup_table(t, testtab);
	int max = 1024 * 1024;
	for (int i = 1; i < max; ++i) {
		bool isnew;
		int *ent = table_putget_testtab(&t, i, &isnew);
		if (!ent) die();
		if (!isnew) return false;
		*ent = i;
	}
	for (int i = 1; i < max; ++i) {
		int *ent = table_get_testtab(&t, i);
		if (!ent) return false;
	}
	return true;
}

TEST("the table should maintain a decent load factor at reasonable sizes") {
	struct table_testtab t;
	setup_table(t, testtab);
	int max = 512 * 1024;
	double avg = 0;
	int navg = 0;
	uint lastsz = t.sz;
	for (int i = 0; i < max; ++i) {
		int *ent = table_put_testtab(&t, i);
		if (!ent) die();
		*ent = i;
		if (t.sz > lastsz) {
			avg += (double)i / (double)lastsz;
			++navg;
		};
		lastsz = t.sz;
	}
	return avg / navg > 0.5;
}

// vi: sw=4 ts=4 noet tw=80 cc=80
