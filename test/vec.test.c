/* This file is dedicated to the public domain. */

#include "test.h"
{.desc = "the dynamic vector implementation"};

#include <stdio.h>
#include "../include/intdefs.h"
#include "../include/vec.h"

static void die(void) { perror("vec.test"); exit(1); }

TEST("pushing and popping from a vector should give the correct value") {
	struct VEC(int) myvec = {0};
	if (!vec_push(&myvec, 20)) die();
	return vec_pop(&myvec) == 20;
}

TEST("pushed values should be poppable in reverse order") {
	struct VEC(int) myvec = {0};
	if (!vec_push(&myvec, 20) || !vec_push(&myvec, 25)) die();
	return vec_pop(&myvec) == 25 && vec_pop(&myvec) == 20;
}

TEST("pushed values should be traversible in regular order") {
	struct VEC(int) myvec = {0};
	if (!vec_push(&myvec, 15) || !vec_push(&myvec, 92)) die();
	return myvec.data[0] == 15 && myvec.data[1] == 92;
}

// vi: sw=4 ts=4 noet tw=80 cc=80
