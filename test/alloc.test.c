/* This file is dedicated to the public domain. */

#include "test.h"
{.desc = "the fast object allocation helpers"};

#include <stdlib.h>
#include <stdbool.h>

static void *fakemalloc_mem = 0;
static bool fakemalloc_called = false;
static void *fakemalloc(size_t n) {
	fakemalloc_called = true;
	return fakemalloc_mem;
}
#define malloc(n) fakemalloc(n)
#include "../include/alloc.h"

struct mystruct {
	int a, b, c, d;
};

#define test1_npblk 16
DEF_PERMALLOC(test1, struct mystruct, test1_npblk)
#define test2_npblk 2
DEF_FREELIST(test2, struct mystruct, test2_npblk)

TEST("permalloc_*() should use all available initial space") {
	for (int i = 0; i < test1_npblk; ++i) if (!permalloc_test1()) return false;
	return true;
}

TEST("permalloc_*() should allocate a new block when space runs out") {
	for (int i = 0; i < test1_npblk; ++i) permalloc_test1();
	char mem[sizeof(struct mystruct) * test1_npblk];
	fakemalloc_mem = mem;
	return permalloc_test1() == fakemalloc_mem;
}

TEST("freelist_alloc_*() should call permalloc_*() only when the free list is empty") {
	struct mystruct *freethis = freelist_alloc_test2();
	// n-per-block + 1 should trigger a block malloc
	for (int i = 0; i < test2_npblk; ++i) freelist_alloc_test2();
	if (!fakemalloc_called) return false;
	fakemalloc_called = false;
	freelist_free_test2(freethis);
	freelist_alloc_test2();
	return !fakemalloc_called;
}

TEST("freelist_free_*() should cause freelist_alloc_*() to reuse the object") {
	struct mystruct *o1 = freelist_alloc_test2();
	struct mystruct *o2 = freelist_alloc_test2();
	for (int i = 0; i < test2_npblk - 1; ++i) freelist_alloc_test2();
	freelist_free_test2(o1);
	freelist_free_test2(o2);
	// might as well also test LIFO order while we're at it
	if (freelist_alloc_test2() != o2) return false;
	if (freelist_alloc_test2() != o1) return false;
	// objects shouldn't be left behind either
	return !freelist_alloc_test2();
}

// vi: sw=4 ts=4 noet tw=80 cc=80
