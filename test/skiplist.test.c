/* This file is dedicated to the public domain. */

#include "test.h"
{.desc = "the skip list implementation"};

#include <stdlib.h>
#include <stdio.h>

#include "../include/skiplist.h"
#include "../include/alloc.h"

DECL_SKIPLIST(static, mylist, struct mystruct, uint, 4)

struct mystruct {
	struct skiplist_hdr_mylist sl_hdr;
	uint a;
};

// this is tested in alloc.test.c
DEF_PERMALLOC(mystruct, struct mystruct, 128)

static inline int mystruct_cmp(struct mystruct *x, uint ya) {
	return x->a > ya ? 1 : x->a < ya ? -1 : 0;
}

static inline struct skiplist_hdr_mylist *mylist_gethdr(struct mystruct *l) {
	return &l->sl_hdr;
}

DEF_SKIPLIST(static, mylist, mystruct_cmp, mylist_gethdr)

TEST("inserting into a skiplist should retain each item in order") {
	struct skiplist_hdr_mylist head = {0};
	for (int i = 0; i < 128; ++i) {
		struct mystruct *n = permalloc_mystruct();
		n->a = i;
		skiplist_insert_mylist(&head, i, n);
	}
	for (int i = 0; i < 128; ++i) {
		struct mystruct *n = skiplist_pop_mylist(&head);
		if (!n || n->a != i) return false;
	}
	return true;
}

TEST("inserting unordered values should allow retreiving them in order") {
	struct skiplist_hdr_mylist head = {0};
	for (int i = 0; i < 128; ++i) {
		struct mystruct *n = permalloc_mystruct();
		n->a = arc4random();
		skiplist_insert_mylist(&head, n->a, n);
	}
	uint highest = 0;
	for (int i = 0; i < 128; ++i) {
		struct mystruct *n = skiplist_pop_mylist(&head);
		if (!n) return false;
		if (n->a >= highest) highest = n->a; else return false;
	}
	return true;
}


TEST("inserted values should be accessible at random") {
	struct skiplist_hdr_mylist head = {0};
	uint values[128];
	arc4random_buf(values, sizeof(values));
	for (int i = 0; i < 128; ++i) {
		struct mystruct *n = permalloc_mystruct();
		n->a = values[i];
		skiplist_insert_mylist(&head, values[i], n);
	}
	for (int i = 0; i < 128; ++i) {
		struct mystruct *n = skiplist_get_mylist(&head, values[i]);
		if (!n || n->a != values[i]) return false;
	}
	// non-existent items should be null
	return !skiplist_get_mylist(&head, -1);
}

TEST("items should be correctly deleted") {
	struct skiplist_hdr_mylist head = {0};
	for (int i = 0; i < 128; ++i) {
		struct mystruct *n = permalloc_mystruct();
		n->a = i;
		skiplist_insert_mylist(&head, i, n);
	}
	skiplist_del_mylist(&head, 6);
	skiplist_del_mylist(&head, 60);
	skiplist_del_mylist(&head, 15);
	skiplist_del_mylist(&head, 120);
	skiplist_del_mylist(&head, 81);
	return !skiplist_get_mylist(&head, 6) &&
			!skiplist_get_mylist(&head, 60) &&
			!skiplist_get_mylist(&head, 15) &&
			!skiplist_get_mylist(&head, 120) &&
			!skiplist_get_mylist(&head, 81);
}

// vi: sw=4 ts=4 noet tw=80 cc=80
