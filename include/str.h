/* This file is dedicated to the public domain. */

#ifndef INC_STR_H
#define INC_STR_H

#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "intdefs.h"
#include "vec0.h"

/*
 * A dynamic string buffer which maintains a null terminator for compatibility
 * with other functions.
 */
struct str VEC0(char);

/*
 * Initialises a string to zero length with a single null terminator character.
 * May also be used to initially allocate the underlying memory after zeroing
 * out the struct.
 */
static inline bool str_clear(struct str *s) { return vec0_clear(s); }

/*
 * Returns the length of the string, not including the null terminator
 * character.
 */
static inline uint str_len(const struct str *s) { return vec0_len(s); }

/*
 * Appends a single character the the end of the string. Returns false if
 * space cannot be allocated.
 */
static inline bool str_appendc(struct str *s, char c) { return vec0_push(s, c); }

/*
 * Appends arbitrary data to the end of the string, possibly including null
 * bytes. The string structure here doesn't mind having nulls embedded in it,
 * although such strings will cause problems if passed directly to functions
 * expecting "C-style" strings.
 *
 * Returns false if space cannot be allocated.
 */
static inline bool str_appendbytes(struct str *s, const char *bytes, uint sz) {
	return vec0_pushall(s, bytes, sz);
}

/*
 * Appends another dynamic string s2 onto the end of s. Returns false if space
 * cannot be allocated.
 */
static inline bool str_appendstr(struct str *s, const struct str *s2) {
	return str_appendbytes(s, s2->data, str_len(s2));
}

/*
 * Appends a C-style null-terminated string to this string. Returns false if
 * space cannot be allocated.
 */
// TODO this could be made generic too one day if we wanted to do that
static inline bool str_append0t(struct str *s, const char *s2) {
	uint oldsz = s->sz;
	for (;;) {
		// copy what we can into whatever space we have left, if we hit a null
		// term we're done, otherwise try to allocate more memory next.
		char *p = s->data + s->sz - 1;
		for (; p - s->data < s->max; ++p, ++s2) {
			*p = *s2;
			if (!*s2) return true;
			++s->sz;
		}
		// don't bother with _vec_make_room, just double the max size.
		// another overflow check *just in case*:
		if (s->max > 1u << 30) { errno = ENOMEM; break; }
		if (!_vec_ensure((struct _vec *)s, 1, s->max * 2)) break;
	}
	// we hit an error, roll back
	s->sz = oldsz;
	return false;
}

/*
 * Checks whether two strings are equal. Faster than just calling strcmp as it
 * can check the lengths first.
 */
static inline bool str_equal(const struct str *s1, const struct str *s2) {
	return s1 == s2 || s1->sz == s2->sz && !memcmp(s1->data, s2->data, s1->sz);
}

// XXX this is now only here for fmt, consider reorganising
static inline char *_str_contigspace(struct str *s, uint sz) {
	if (s->max - s->sz < sz && !_vec_make_room((struct _vec*)s, 1, sz)) {
		return 0;
	}
	else {
		uint oldsz = str_len(s);
		s->sz += sz;
		s->data[s->sz - 1] = '\0';
		return s->data + oldsz;
	}
}

#endif

// vi: sw=4 ts=4 noet tw=80 cc=80
