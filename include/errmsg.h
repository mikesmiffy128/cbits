/* This file is dedicated to the public domain. */

#ifndef INC_ERRMSG_H
#define INC_ERRMSG_H

#include <errno.h>

void _errmsg_warn(const char *s1, const char *s2, const char *s3,
		const char *s4, const char *s5, const char *s6, const char *s7,
		const char *s8, int err);
_Noreturn void _errmsg_die(const char *s1, const char *s2, const char *s3,
		const char *s4, const char *s5, const char *s6, const char *s7,
		const char *s8, int err, int status);

#define _errmsgwarnargs(e, s1, s2, s3, s4, s5, s6, s7, s8, ...) \
	_errmsg_warn(s1, s2, s3, s4, s5, s6, s7, s8, e)
#define _errmsgdieargs(e, s, s1, s2, s3, s4, s5, s6, s7, s8, ...) \
	_errmsg_die(s1, s2, s3, s4, s5, s6, s7, s8, e, s)

/*
 * Print an error message to file descriptor 2 (stderr). This message consists
 * of the name of the program, a colon and space, the concatenation of all the
 * strings passed to the function, another colon and space and a description of
 * the last error as given by errorstring().
 */
#define errmsg_warn(...) \
	_errmsgwarnargs(errno, __VA_ARGS__, 0, 0, 0, 0, 0, 0, 0, 0)

/*
 * Print an error message to file descriptor 2 (stderr). This message consists
 * of the name of the program, a colon and space and the concatenation of all
 * the strings passed to the function.
 */
#define errmsg_warnx(...) \
	_errmsgwarnargs(-1, __VA_ARGS__, 0, 0, 0, 0, 0, 0, 0, 0)

/*
 * Does the same as errmsg_warn, then exits with the given status code.
 */
#define errmsg_die(status, ...) \
	_errmsgdieargs(errno, status, __VA_ARGS__, 0, 0, 0, 0, 0, 0, 0, 0)

/*
 * Does the same as errmsg_warnx, then exits with the given status code.
 */
#define errmsg_diex(status, ...) \
	_errmsgdieargs(-1, status, __VA_ARGS__, 0, 0, 0, 0, 0, 0, 0, 0)

/*
 * These are some common message prefixes intended to facilitate consistent
 * error messages.
 */
extern const char msg_note[],  msg_warn[], msg_temp[],
				  msg_error[], msg_crit[], msg_fatal[];

#endif

// vi: sw=4 ts=4 noet tw=80 cc=80
