/* This file is dedicated to the public domain. */

#ifndef INC_PATH_H
#define INC_PATH_H

#include <stdbool.h>

/*
 * Returns true if a program is already a qualified path (containing at least
 * one slash), false if it's a single word which should be looked up in a PATH
 * variable (e.g. using path_search)
 */
bool path_isfull(const char *prog);

/*
 * Returns the full path to a named program on the given path (a colon separated
 * list of directories, e.g. getenv("PATH"), or a null pointer if no program
 * is found. In the latter case, errno is set to indicate the most interesting
 * error that was encountered.
 */
const char *path_search(const char *path, const char *prog);

#endif

// vi: sw=4 ts=4 noet tw=80 cc=80

