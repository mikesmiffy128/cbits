/* This file is dedicated to the public domain. */

#ifndef INC_RUNE_H
#define INC_RUNE_H

#include "intdefs.h"

/*
 * A rune is simply the Plan 9 and Go terminology for a Unicode code point, and
 * provies a more convenient way to refer to a code point than something like
 * "char32_t".
 */
typedef uint rune;

/*
 * Prints the (supposed) column width of a given code point. This is 1 for most
 * characters, but some will be 2 or even 0. This doesn't handle clusters or
 * zwj, making it probably less useful on its own. It is advised to use the
 * functions in strwidth.h for most purposes.
 */
int runewidth(rune r);

#endif

// vi: sw=4 ts=4 noet tw=80 cc=80
