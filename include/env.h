/* This file is dedicated to the public domain. */

#ifndef INC_ENV_H
#define INC_ENV_H

#include <stdbool.h>

#include "vec.h"

/*
 * An env is a null-terminated list of environment variables that can be passed
 * to e.g. execve(). Returns true on success, false on failure to allocate
 * memory or possibly some other error indicated in errno.
 */
struct env VEC(char *);

/*
 * Initialises an env using the current process environment variables (environ).
 * Unsets any existing values in the env. Returns true on success, false on
 * failure to allocate memory or possibly some other error indicated in errno.
 */
bool env_init(struct env *v);

/*
 * Initialises an env, completely emptying it of any variables. Returns true on
 * success, false on failure to allocate memory or possibly some other error
 * indicated in errno.
 */
bool env_initempty(struct env *v);

/*
 * Returns either the value of a variable in the env, or a null pointer if it
 * doesn't exist.
 */
const char *env_get(struct env *v, const char *var);

/*
 * Inserts a value into the env, replacing any existing variable of the same
 * name. Returns true on success, false on failure to allocate memory or
 * possibly some other error indicated in errno.
 */
bool env_put(struct env *v, const char *var, const char *val);

#endif

// vi: sw=4 ts=4 noet tw=80 cc=80
