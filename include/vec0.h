/* This file is dedicated to the public domain. */

#ifndef INC_VEC0_H
#define INC_VEC0_H

#include <stdbool.h>
#include <stdlib.h>

#include "vec.h"

/*
 * A generalised dynamic string with a null terminator. Forms the basis of str.
 *
 * Usage: struct VEC0(my_type) myvec = {0};
 * Or: struct myvec VEC(my_type);
 * Or: typedef struct VEC(my_type) myvec;
 */
#define VEC0(t) VEC(t)

// helper for various subsequent things
#define _vec0_put0(v) \
	(memset((v)->data + (v)->sz - 1, 0, sizeof(*(v)->data)), true)

/*
 * Initialises a vec0 to zero length with a single null terminator value.
 * May also be used to initially allocate the underlying memory after zeroing
 * out the struct.
 */
#define vec0_clear(v) ( \
	((v)->max > 0 || _vec_ensure((struct _vec *)(v), sizeof(*(v)->data), 16)) && \
		((v)->sz = 1, _vec0_put0(v)) \
)

/*
 * Returns the length, excluding the null terminator.
 */
#define vec0_len(v) ((v)->sz - 1)

/* These are essentially the same as for vec */
#define vec0_push(v, val)		 (_vec_push(v, val, 1) && _vec0_put0(v))
#define vec0_pushall(v, vals, n) (_vec_pushall(v, vals, n, 1) && _vec0_put0(v))

#endif

// vi: sw=4 ts=4 noet tw=80 cc=80
