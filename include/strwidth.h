/* This file is dedicated to the public domain. */

#include "intdefs.h"

/*
 * Returns the expected number of terminal columns occupied by the given string,
 * ignoring the actual width of the terminal.
 */
uint strwidth(const char *s);

/*
 * Does the same as strwidth, but accounts for common escape codes e.g. (ANSI
 * colour codes) by parsing them out so that the length is not affected.
 */
uint strwidth_colour(const char *s);

// vi: sw=4 ts=4 noet tw=80 cc=80
