/*
 * Copyright © 2021 Michael Smith <mikesmiffy128@gmail.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef INC_TABLE_H
#define INC_TABLE_H

#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include "intdefs.h"

#ifdef __GNUC__
#define _table_unused __attribute__((unused)) // heck off gcc
#else
#define _table_unused
#endif

struct _table {
	uint sz;
	uint transact;
	void *data;
	u64 *flags;
};

static inline bool _table_ispresent(u64 *flags, uint i) {
	return !!(flags[i >> 6] & 1ull << (i & 63));
}

static inline void _table_setpresent(u64 *flags, uint i) {
	flags[i >> 6] |= 1ull << (i & 63);
}

static inline void _table_unsetpresent(u64 *flags, uint i) {
	flags[i >> 6] &= ~(1ull << (i & 63));
}

static inline bool _table_init(struct _table *t, uint valsz) {
	enum { SZ = 32 };
	void *data = reallocarray(0, SZ, valsz);
	if (!data) return false;
	u64 *flags = calloc(1, sizeof(u64));
	if (!flags) { free(data); return false; }
	t->sz = SZ;
	t->data = data;
	t->flags = flags;
	return true;
}

// the hashtable is grown if an inserted key would take too many probes, but to
// avoid birthday paradox-related issues, be more lenient for larger tables
static inline int _table_maxprobe(uint sz) { return ffs(sz) * 2 - 8; }

/*
 * Declares the hashtable type struct table_hdr##name, but none of the
 * associated functions. Use when the structure needs to be passed around in
 * some way but actual operations on the table are a private implementation
 * detail. Otherwise, see DECL_TABLE below.
 */
#define DECL_TABLE_TYPE(name, ktype, dtype) \
typedef ktype _table_kt_##name; \
typedef dtype _table_dt_##name; \
struct table_##name { \
	uint sz; \
	uint transact; \
	dtype *data; \
	u64 *flags; \
};

/*
 * Declares the hashtable type struct table_##name, and associated functions
 * table_{init,put,get,del}_##name. A single occurence of DEF_TABLE is required
 * to actually implement the functions.
 *
 * This macro implies DECL_TABLE_TYPE (both should not be used).
 *
 * mod should be either static or extern.
 *
 * ktype is the key which gets hashed for lookups and dtype is the internal
 * structure of the hashtable (which should contain or relate to the key in some
 * way). The table embeds entries of dtype directly - it does not necessarily
 * have to use pointers, but dtype itself will often be a pointer if not a very
 * small struct in order to keep the table cache-friendly.
 *
 * The put, get and del functions all return a pointer to the dtype structure
 * inserted or located in the hashtable. A deleted entry may be overwritten
 * later, but is valid immediately after removal. This means, for instance, an
 * item may be taken out of a set and then processed.
 *
 * The put function will return a null pointer if the item could not be
 * inserted, and get and del return a null pointer if no such item exists in the
 * table.
 *
 * The putget function does the same as put, but indicates whether or not the
 * item was newly created.
 */
#define DECL_TABLE(mod, name, ktype, dtype) \
DECL_TABLE_TYPE(name, ktype, dtype) \
\
static inline bool table_init_##name(struct table_##name *t) { \
	return _table_init((struct _table *)t, sizeof(dtype)); \
} \
\
_table_unused mod dtype *table_put_##name(struct table_##name *t, ktype k); \
_table_unused mod dtype *table_putget_##name(struct table_##name *t, ktype k, \
		bool *isnew); \
_table_unused mod dtype *table_putget_transact_##name(struct table_##name *t, \
		ktype k, bool *isnew); \
_table_unused mod void table_transactcommit_##name(struct table_##name *t); \
_table_unused mod dtype *table_get_##name(struct table_##name *t, ktype k); \
_table_unused mod dtype *table_del_##name(struct table_##name *t, ktype k);

/*
 * Implements the functions corresponding to a hashtable - must come after
 * DECL_TABLE with the same modifier and name.
 *
 * hfunc should be a hash function which takes the key type and returns an
 * unsigned int - see basichashes.h for simple examples.
 *
 * eqfunc should implement equality checking of two keys - a definition would
 * look like:
 * bool eq(ktype a, ktype b);
 */
#define DEF_TABLE(mod, name, hfunc, eqfunc, kmembfunc) \
static inline bool _table_grow_##name(struct table_##name *t) { \
	uint newsz = t->sz; \
	/* 64-bit sizes are unsupported because who needs a 4GiB hashtable!? */ \
r:	if (newsz == 1u << 31) { errno = ENOMEM; return false; } \
	newsz <<= 1; \
	_table_dt_##name *newdata = reallocarray(0, newsz, sizeof(*newdata)); \
	if (!newdata) return false; \
	u64 *newflags = calloc(newsz >> 6, sizeof(u64)); /* note: size >= 64 */ \
	if (!newflags) { free(newdata); return false; } \
	/* rehash! */ \
	uint mask = newsz - 1; \
	int maxprobe = _table_maxprobe(newsz); \
	for (uint i = 0; i < t->sz; ++i) { \
		if (_table_ispresent(t->flags, i)) { \
			uint idx = hfunc(kmembfunc(t->data + i)); \
			for (int off = 0; off < maxprobe; ++off) { \
				uint masked = (idx + off) & mask; \
				if (!_table_ispresent(newflags, masked)) { \
					_table_setpresent(newflags, masked); \
					memcpy(newdata + masked, t->data + i, sizeof(*newdata)); \
					goto c; \
				} \
			} \
			/* collision again, gotta go even more bigger! */ \
			free(newdata); \
			free(newflags); \
			goto r; \
		} \
c:;	} \
	free(t->data); t->data = newdata; \
	free(t->flags); t->flags = newflags; \
	t->sz = newsz; \
	return true; \
} \
\
_table_unused \
mod _table_dt_##name *table_put_##name(struct table_##name *t, \
		_table_kt_##name k) { \
	uint idx = hfunc(k); \
	for (;;) { \
		uint mask = t->sz - 1; \
		int maxprobe = _table_maxprobe(t->sz); \
		for (int off = 0; off < maxprobe; ++off) { \
			uint masked = (idx + off) & mask; \
			/* if not present, probe further for stuff to replace */ \
			if (!_table_ispresent(t->flags, masked)) { \
				while (++off < maxprobe) { \
					uint masked2 = (idx + off) & mask; \
					if (_table_ispresent(t->flags, masked2) && \
							eqfunc(kmembfunc(t->data + masked2), k)) { \
						return t->data + masked2; \
					} \
				} \
				/* nothing else there, use what we already found */ \
				_table_setpresent(t->flags, masked); \
				return t->data + masked; \
			} \
			/* if already present, replace */ \
			if (eqfunc(kmembfunc(t->data + masked), k)) { \
				return t->data + masked; \
			} \
		} \
		/* otherwise grow table to avoid too many probes - could technically
		 * happen more than once in a row but almost never will */ \
		if (!_table_grow_##name(t)) return 0; \
	} \
} \
\
_table_unused \
mod _table_dt_##name *table_putget_##name(struct table_##name *t, \
		_table_kt_##name k, bool *isnew) { \
	/* XXX this is almost just a copypaste */ \
	uint idx = hfunc(k); \
	for (;;) { \
		uint mask = t->sz - 1; \
		int maxprobe = _table_maxprobe(t->sz); \
		for (int off = 0; off < maxprobe; ++off) { \
			uint masked = (idx + off) & mask; \
			if (!_table_ispresent(t->flags, masked)) { \
				while (++off < maxprobe) { \
					uint masked2 = (idx + off) & mask; \
					if (_table_ispresent(t->flags, masked2) && \
							eqfunc(kmembfunc(t->data + masked2), k)) { \
						*isnew = false; \
						return t->data + masked2; \
					} \
				} \
				_table_setpresent(t->flags, masked); \
				*isnew = true; \
				return t->data + masked; \
			} \
			if (eqfunc(kmembfunc(t->data + masked), k)) { \
				*isnew = false; \
				return t->data + masked; \
			} \
		} \
		if (!_table_grow_##name(t)) return 0; \
	} \
} \
\
_table_unused \
mod _table_dt_##name *table_putget_transact_##name(struct table_##name *t, \
		_table_kt_##name k, bool *isnew) { \
	t->transact = -1u; /* indices can't be this high */ \
	/* XXX this is also almost just a copypaste */ \
	uint idx = hfunc(k); \
	for (;;) { \
		uint mask = t->sz - 1; \
		int maxprobe = _table_maxprobe(t->sz); \
		for (int off = 0; off < maxprobe; ++off) { \
			uint masked = (idx + off) & mask; \
			if (!_table_ispresent(t->flags, masked)) { \
				while (++off < maxprobe) { \
					uint masked2 = (idx + off) & mask; \
					if (_table_ispresent(t->flags, masked2) && \
							eqfunc(kmembfunc(t->data + masked2), k)) { \
						*isnew = false; \
						return t->data + masked2; \
					} \
				} \
				t->transact = masked; \
				*isnew = true; \
				return t->data + masked; \
			} \
			if (eqfunc(kmembfunc(t->data + masked), k)) { \
				*isnew = false; \
				return t->data + masked; \
			} \
		} \
		/* otherwise grow table to avoid too many probes - could technically
		 * happen more than once in a row but almost never will */ \
		if (!_table_grow_##name(t)) return 0; \
	} \
} \
\
_table_unused \
mod void table_transactcommit_##name(struct table_##name *t) { \
	if (t->transact != -1u) _table_setpresent(t->flags, t->transact); \
} \
\
_table_unused \
mod _table_dt_##name *table_get_##name(struct table_##name *t, \
		_table_kt_##name k) { \
	uint idx = hfunc(k); \
	uint mask = t->sz - 1; \
	int maxprobe = _table_maxprobe(t->sz); \
	for (int off = 0; off < maxprobe; ++off) { \
		uint masked = (idx + off) & mask; \
		if (_table_ispresent(t->flags, masked) && \
				eqfunc(kmembfunc(t->data + masked), k)) { \
			return t->data + masked; \
		} \
	} \
	return 0; \
} \
\
_table_unused \
mod _table_dt_##name *table_del_##name(struct table_##name *t, \
		_table_kt_##name k) { \
	uint idx = hfunc(k); \
	uint mask = t->sz - 1; \
	int maxprobe = _table_maxprobe(t->sz); \
	for (int off = 0; off < maxprobe; ++off) { \
		uint masked = (idx + off) & mask; \
		if (_table_ispresent(t->flags, masked) && \
				eqfunc(kmembfunc(t->data + masked), k)) { \
			_table_unsetpresent(t->flags, masked); \
			return t->data + masked; \
		} \
	} \
	return 0; \
}

/*
 * The simplest equality check, simply a direct comparison.
 */
#define table_ideq(a, b) ((a) == (b))

/*
 * The simplest hash member getter, simply the entry itself.
 */
#define table_idmemb(x) (x)

/*
 * A hashtable member getter which simply dereferences the pointer to get the
 * underlying (scalar) value.
 */
#define table_scalarmemb(x) (*(x))

#define TABLE_FOREACH_IDX(loopvar, t) \
	for (uint loopvar = 0; loopvar < (t)->sz; ++loopvar) \
		if (_table_ispresent((t)->flags, loopvar)) // { stuff }

#define TABLE_FOREACH_PTR(ptrvar, name, t) \
	/* HACK: abusing this janky for loop to create a scope without braces */ \
	for (_table_dt_##name *ptrvar = (t)->data; ptrvar; ptrvar = 0) \
		TABLE_FOREACH_IDX(_table_i_##ptrvar, t) \
			/* hack pt2 - using if to assign a variable while still allowing
			   either a statement or braces to be "attached" to the foreach
			   (and ensuring break/continue still work) */ \
			if (ptrvar = (t)->data + _table_i_##ptrvar, 1)

#define table_delidx(t, idx) _table_unsetpresent((t)->flags, idx)
#define table_delptr(t, ptr) _table_unsetpresent((t)->flags, (ptr) - (t)->data)

#endif

// vi: sw=4 ts=4 noet tw=80 cc=80
