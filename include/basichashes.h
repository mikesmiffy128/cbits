/* This file is dedicated to the public domain. */

#ifndef INC_HASH_H
#define INC_HASH_H

#include "intdefs.h"

/*
 * A collection of simple and fast hash functions for use with table.h.
 */

static inline uint hash_int(uint x) {
	// from Chris Wellons' "Hash Prospector"
	x ^= x >> 16;
	x *= 0x7feb352d;
	x ^= x >> 15;
	x *= 0x846ca68b;
	x ^= x >> 16;
	return x;
}

static inline uint hash_vlong(uvlong x) { return hash_int(x >> 32 ^ x); } // meh

static inline uint hash_long(ulong x) {
	if    (sizeof(long) == sizeof(int))		return hash_int(x);
	else /*sizeof(long) == sizeof(vlong)*/	return hash_vlong(x);
}

static inline uint hash_ptr(const void *x) {
	// NOTE: assuming long and pointer are the same size, valid assumption
	// for every Unix nowadays, but not for example Windows.
	return hash_long((ulong)x);
}

/*
 * Note: these are exposed like this to allow constructing hash functions over
 * more complex structures
 */
// FNV-1a
static const uint HASH_ITER_INIT = 2166136261;
static inline uint hash_iter(uint h, char c) { return (h ^ c) * 16777619; }

static inline uint hash_iter_str(uint h, const char *s) {
	while (*s) h = hash_iter(h, *s++);
	return h;
}

static inline uint hash_iter_bytes(uint h, const char *bytes, uint len) {
	for (const char *p = bytes; p - bytes < len; ++p) h = hash_iter(h, *p);
	return h;
}

static inline uint hash_str(const char *s) {
	return hash_iter_str(HASH_ITER_INIT, s);
}

static inline uint hash_bytes(const char *bytes, uint len) {
	return hash_iter_bytes(HASH_ITER_INIT, bytes, len);
}

#endif

// vi: sw=4 ts=4 noet tw=80 cc=80
