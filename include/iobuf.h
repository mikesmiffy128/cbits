/*
 * Copyright © 2021 Michael Smith <mikesmiffy128@gmail.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef INC_IOBUF_H
#define INC_IOBUF_H

#include "intdefs.h"
#include "str.h"

/* NOTE: these structs should be treated as opaque */

struct ibuf {
	int fd;
	uint sz;
	uint r, w;
	char b[];
};

struct obuf {
	int fd;
	uint sz;
	uint n;
	char b[];
};

#define _BUF_SPACE(which, n) union { \
	char x[sizeof(struct which) + (n)]; \
	struct which s; \
}

/*
 * Initialisers for IO buffers for a given fd and buffer size. Note: should not
 * be used on the stack.
 */
#define IBUF(fd, space) ( \
	&(_BUF_SPACE(ibuf, space)){.s.f##d = (fd), .s.sz = (space)}.s \
)
#define OBUF(fd, space) \
	&(_BUF_SPACE(obuf, space)){.s.f##d = (fd), .s.sz = (space)}.s

extern struct ibuf *const buf_in;
extern struct obuf *const buf_out, *const buf_err;

/*
 * Returns how many more bytes can be written to the buffer without flushing.
 */
uint obuf_avail(const struct obuf *buf);

/*
 * Fills the buffer space from x up to length n, stopping if the space is full.
 * Does not automatically flush, and returns the actual number of bytes copied.
 */
uint obuf_fill(struct obuf *buf, const char *x, uint n);

/*
 * Flushes the buffered bytes out to the underlying file. Returns true on
 * success, false on error.
 */
bool obuf_flush(struct obuf *buf);

/*
 * Clears the output buffer, discarding anything pending to be written.
 */
void obuf_reset(struct obuf *buf);

/*
 * The obuf_put* functions output their given arguments, flushing whenever the
 * buffer gets full. They do not perform additional flushing; data is only
 * written out when necessary.
 */
bool obuf_putc(struct obuf *buf, char c);
bool obuf_putbytes(struct obuf *buf, const char *bytes, uint n);
bool obuf_putstr(struct obuf *buf, const struct str *s);
bool obuf_put0t(struct obuf *buf, const char *s);

#define IOBUF_EOF -2

/*
 * Reads a single character, buffering more for later if possible. Returns the
 * given character 0-extended to a short on success, IOBUF_EOF if EOF has been
 * reached, and -1 on failure.
 */
short ibuf_getc(struct ibuf *buf);

/*
 * Reads a block of bytes at once, buffering more for later if possible. Returns
 * the number of bytes read on success, 0 if EOF has been reached, or -1 on
 * failure.
 */
int ibuf_getbytes(struct ibuf *buf, void *bytes, int n);

/*
 * Reads in a string, delimited by term, and appends to the given string buffer.
 * The terminator is included in the result, unless there was an unexpected EOF.
 * Returns the number of bytes read on success, 0 if EOF has been reached, or -1
 * on failure.
 */
int ibuf_getstr(struct ibuf *buf, struct str *s, char term);

#endif

// vi: sw=4 ts=4 noet tw=80 cc=80
