/* This file is dedicated to the public domain. */

#ifndef INC_ERRORSTRING_H
#define INC_ERRORSTRING_H

/*
 * Returns a developer-friendly, English-language description of the value of
 * errno. This description is consistent between platforms, and since it is
 * unlocalised, issues with deadlocks in signal handlers such as those in glibc
 * are sidestepped.
 */
const char *errorstring(int e);

#endif

// vi: sw=4 ts=4 noet tw=80 cc=80
