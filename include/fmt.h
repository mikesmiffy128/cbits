/*
 * Copyright © 2021 Michael Smith <mikesmiffy128@gmail.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef INC_FMT_H
#define INC_FMT_H

#include "intdefs.h"
#include "iobuf.h"
#include "str.h"

// tcc isn't a fully compliant C compiler, but it's a really *fast* C compiler,
// so it's considered worth the effort in this one file to support it
#ifdef __TINYC__
#define _fmt_static
#else
#define _fmt_static static
#endif

/*
 * The fmt_buf_ functions write formatted text to an output buffer (see iobuf.h).
 *
 * u64, s64, u32 and s32 functions write out integer types, and str writes out
 * dynamic strings (see str.h).
 *
 * Each function also has a "pad" variant, which left-pads the resulting output.
 *
 * The return value of every function is the length of the output in bytes.
 * Outputting the actual visible width of the text is being considered, but is
 * not the current behaviour. If an error occurs, the return value is -1.
 */
int fmt_buf_u64(struct obuf *buf, u64 n);
int fmt_buf_u64pad(struct obuf *buf, u64 n, char c, uint mlen);
int fmt_buf_s64(struct obuf *buf, s64 n);
int fmt_buf_s64pad(struct obuf *buf, s64 n, char c, uint mlen);
int fmt_buf_u32(struct obuf *buf, u32 n);
int fmt_buf_u32pad(struct obuf *buf, u32 n, char c, uint mlen);
int fmt_buf_s32(struct obuf *buf, s32 n);
int fmt_buf_s32pad(struct obuf *buf, s32 n, char c, uint mlen);
int fmt_buf_str(struct obuf *buf, const struct str *s);
int fmt_buf_strpad(struct obuf *buf, const struct str *s, char c, uint mlen);

/*
 * The fmt_str_ functions work the same as the fmt_buf_ functions but they
 * append to a dynamic string instead of an output buffer.
 */
int fmt_str_u64(struct str *s, u64 n);
int fmt_str_u64pad(struct str *s, u64 n, char c, uint mlen);
int fmt_str_s64(struct str *s, s64 n);
int fmt_str_s64pad(struct str *s, s64 n, char c, uint mlen);
int fmt_str_u32(struct str *s, u32 n);
int fmt_str_u32pad(struct str *s, u32 n, char c, uint mlen);
int fmt_str_s32(struct str *s, s32 n);
int fmt_str_s32pad(struct str *s, s32 n, char c, uint mlen);
int fmt_str_str(struct str *s, const struct str *s2);
int fmt_str_strpad(struct str *s, const struct str *s2, char c, uint mlen);

/*
 * The fmt_fixed_ functions write formatted numbers to a character array,
 * without doing any allocation or bounds checking. The array must have a size
 * of at least 11 for 32-bit values (or 10 for unsigned) and at least 20 for
 * 64-bit values, not including a null terminator - a null-terminator is NOT
 * written by these functions.
 */
int fmt_fixed_u64(char buf[_fmt_static 20], u64 n);
int fmt_fixed_s64(char buf[_fmt_static 20], s64 n);
int fmt_fixed_u32(char buf[_fmt_static 10], u32 n);
int fmt_fixed_s32(char buf[_fmt_static 11], s32 n);

#endif

// vi: sw=4 ts=4 noet tw=80 cc=80
