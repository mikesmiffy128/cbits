/*
 * Copyright © 2020 Michael Smith <mikesmiffy128@gmail.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef INC_ALLOC_H
#define INC_ALLOC_H

#include <stdlib.h>

#define _DEF_PERMALLOC(name, type, npblk, fname, malloc) \
static type _permalloc_space_##name[npblk]; \
static type *_permalloc_blk_##name = _permalloc_space_##name; \
static type *_permalloc_p_##name = _permalloc_space_##name; \
static type *fname(void) { \
	if (_permalloc_p_##name - _permalloc_blk_##name < (npblk)) { \
		return _permalloc_p_##name++; \
	} \
	type *new = malloc((npblk) * sizeof(type)); \
	if (new) { _permalloc_blk_##name = new; _permalloc_p_##name = new + 1; } \
	return new; \
}

/*
 * Creates a "permanent allocator" - an extremely fast typesafe allocator which
 * is really just a glorified static array, but with a real memory allocator
 * backing it in the event that the preallocated memory is used up. This is
 * designed for allocations that never need to be freed since they either remain
 * in use forever or the program is short-lived enough that calling free() would
 * just waste cycles.
 */
#define DEF_PERMALLOC(name, type, npblk) \
	_DEF_PERMALLOC(name, type, npblk, permalloc_##name, malloc)

#define _DEF_FREELIST(name, type, npblk, fnamea, fnamef, malloc) \
union _freelist_##name { \
	type x; \
	union _freelist_##name *next; \
}; \
_DEF_PERMALLOC(_freelist_##name, union _freelist_##name, npblk, \
		_freelist_permalloc_##name, malloc) \
static union _freelist_##name *_freelist_##name = 0; \
static type *fnamea(void) { \
	if (_freelist_##name) { \
		union _freelist_##name *ret = _freelist_##name; \
		_freelist_##name = _freelist_##name->next; \
		return &ret->x; \
	} \
	/* amusing fact: UBSan complains about `->` here, so just cast instead */ \
	/* return &_freelist_permalloc_##name()->x; */ \
	return (type *)_freelist_permalloc_##name(); \
} \
static void fnamef(type *p) { \
	if (!p) return; \
	union _freelist_##name *up = (union _freelist_##name *)p; \
	up->next = _freelist_##name; \
	_freelist_##name = up; \
}

/*
 * Creates a perma-alloc-backed freelist allocator, which frees objects by
 * building a linked list and uses this list for subsequent allocations.
 * Useful in programs which rapidly allocate and deallocate many instances of
 * a structure, as the underlying memory is never freed and fragmentation within
 * an allocated chunk is exactly zero. And of course, a typical allocation
 * essentially consists of a single pointer dereference.
 */
#define DEF_FREELIST(name, type, npblk) \
	_DEF_FREELIST(name, type, npblk, freelist_alloc_##name, \
			freelist_free_##name, malloc)

#endif

// vi: sw=4 ts=4 noet tw=80 cc=80
