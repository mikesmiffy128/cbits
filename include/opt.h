/* This file is dedicated to the public domain. */

#include <stdlib.h>

#include "errmsg.h"
#include "iobuf.h"

/*
 * Prints usage information and exits with a given return code.
 */
static _Noreturn void usagex(int ret);

/*
 * Prints usage information and exits with the status code 1.
 */
static _Noreturn void usage(void);

/*
 * Defines the usage string of the program. Must be specified before main() if
 * FOR_OPTS is used to parse arguments.
 */
#define USAGE(s) \
static const char *_usage; \
static _Noreturn void usagex(int ret) { \
	obuf_put0t(buf_err, "usage: "); \
	obuf_put0t(buf_err, getprogname()); \
	obuf_putc(buf_err, ' '); \
	obuf_put0t(buf_err, _usage); \
	obuf_putc(buf_err, '\n'); \
	obuf_flush(buf_err); \
	exit(ret); \
} \
static _Noreturn void usage(void) { usagex(1); } \
static const char *_usage = "" s

static _Noreturn void _opt_bad(const char *s, char c) {
	obuf_put0t(buf_err, getprogname());
	obuf_put0t(buf_err, ": ");
	obuf_put0t(buf_err, s);
	obuf_putc(buf_err, c);
	obuf_putc(buf_err, '\n');
	usage();
}

/*
 * Parses POSIX-compliant command-line options. Takes the names of argc and argv
 * plus a braced code block containing switch cases matching each option
 * character. The default case is already provided and causes an error message
 * to be printed and the program to exit.
 */
#define FOR_OPTS(argc, argv, CODE) do { \
	--(argc); \
	++(argv); \
	for (const char *_p = *(argv), *_p1; *(argv); _p = *++(argv), --(argc)) { \
		(void)(_p1); /* avoid unused warnings if OPTARG isn't used */ \
		if (*_p != '-' || !*++_p) break; \
		if (*_p == '-' && !*(_p + 1)) { ++(argv); --(argc); break; } \
		while (*_p) { \
			switch (*_p++) { \
				default: \
					_opt_bad("invalid option: -", *(_p - 1)); \
					usage(); \
				CODE \
			} \
		} \
	} \
} while(0)

/*
 * Produces the string given as a parameter to the current option - must only be
 * used inside one of the cases given to FOR_OPTS. If there is no parameter
 * given, prints an error message and exits.
 */
#define OPTARG(argc, argv) \
	(*_p ? (_p1 = _p, _p = "", _p1) : (*(--(argc), ++(argv)) ? *(argv) : \
		(_opt_bad("missing argument for option -", *(_p - 1)), usage(), \
			(char *)0)))

// vi: sw=4 ts=4 noet tw=80 cc=80
