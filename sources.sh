# This file is dedicated to the public domain.

sources="
env.c
errmsg.c
errorstring.c
fmt.c
iobuf.c
path.c
rune.c
strwidth.c
utf.c
"
host_sources="
iobuf.c
errmsg.c
errorstring.c
fmt.c
"
cmd_sources="
mkstr.c
mkarray.c
"
tests="
alloc.test.c
opt.test.c
vec.test.c
skiplist.test.c
str.test.c
table.test.c
utf.test.c
"

# vi: sw=4 ts=4 noet tw=80 cc=80
